var express = require('express');
var router = express.Router();
const fetch = require('node-fetch');

const UserModel = require('../models/users');

/* GET user basic information (cpf, email and password) */
router.get('/:email', function(req, res, next) {
  UserModel.findOne({email: req.params.email})
  .then(function(user) {
    if (user == null) {
      res.status(404).send({
        message: "Não há usuário cadastrado com o email fornecido."
      });
      return;
    }

    res.send(user);
  })
});

/* POST add a new user */
router.post('/add', function(req, res, next) {
  var required = ['cpf', 'email', 'senha', 'nome', 'dataDeNascimento',
                  'telefone'];

  for (var i = 0; i < required.length; i++) {
    if (req.body[required[i]] === undefined) {
      res.status(400).send({
        message: "Dado não fornecido: " + required[i]
      });
      return;
    }
  }

  var cpf = req.body.cpf;
  var email = req.body.email;
  var senha = req.body.senha;
  var nome = req.body.nome;
  var dataDeNascimento = req.body.dataDeNascimento;
  var telefone = req.body.telefone;

  var user = new UserModel({ cpf: cpf, email: email, senha: senha, isAdmin: false });
  user.save()
  .then(function(data) {
    const response = fetch('http://localhost:3002/register', {
      method: 'POST',
      body: JSON.stringify({
        cpf: cpf,
        email: email,
        senha: senha,
        nome: nome,
        dataDeNascimento: dataDeNascimento,
        telefone: telefone,
        idGrupo: "GioGio"
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(function(response) {
      var jsonResponse = response.json();
      if (response.status != 200) {
        res.status(response.status).send(jsonResponse.message);
        return;
      }
      return jsonResponse;
    }).then(function(data) {
      res.status(200).send(data);
      return;
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
