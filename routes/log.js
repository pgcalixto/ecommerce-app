'use strict';
var express = require('express');
var router = express.Router();

const LogModel = require('../models/logs');

/* POST new log entry */
router.post('/', function (req, res, next) {

  // check header
  if (req.headers['content-type'] !== 'application/json') {
    res.status(400).send({
      message: "Content-type deve ser application/json."
    });
    return;
  }

  // get tag text and tag
  let text = req.body.text;
  let tag = req.body.tag;
  let timestamp = req.body.timestamp;

  // check if required fields were passed
  if(text === undefined || tag === undefined || timestamp === undefined) {
    let undef = (!text ? 'text' : (!tag ? 'tag' : 'timestamp'));
    res.status(400).send({
      message: "Campo '" + undef +"' não definido"
    });
    return;
  }

  // create log entry
  let log = new LogModel({
    text: text,
    tag: tag,
    timestamp: timestamp
  });

  // save log entry
  log.save()
  .then(function(data) {
    res.status(202).send({
      message: "Logged '" + text + "' with tag '" + tag + "'"
    });
  });
});

/* GET logs with specified tag */
router.get('/tag/:tag', function (req, res, next) {

  // get tag
  let tag = req.params.tag;

  // find and return logs with specified tag
  LogModel.find({tag: tag}, {_id: 0, text: 1, tag: 1, timestamp: 1})
  .then(function(logs) {
    // sort logs by timestamp
    logs = logs.sort((a, b) => {return a.timestamp - b.timestamp})

    // return logs
    res.status(200).send(logs);
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

/* GET all logs */
router.get('/all', function (req, res, next) {

  // find and return logs with specified tag
  LogModel.find({}, {_id: 0, text: 1, tag: 1, timestamp: 1})
  .then(function(logs) {
    // sort logs by timestamp
    logs = logs.sort((a, b) => {return a.timestamp - b.timestamp})

    // return logs
    res.status(200).send(logs);
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
