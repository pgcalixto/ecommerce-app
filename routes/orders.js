var express = require('express');
var router = express.Router();
const fetch = require('node-fetch');

const OrderModel = require('../models/orders');

/* GET all orders from an user */
router.get('/:cpf', function(req, res, next) {
  OrderModel.find({cpf: req.params.cpf})
  .then(function (response) {
    var products = []
    for (let i = 0; i < response.length; i++) {
      let productList = []

      /* Generate product list, containing only id and quantity */
      for (let j = 0; j < response[i].productList.length; j++) {
        productList.push({
          productId: response[i].productList[j].productId,
          quantity: response[i].productList[j].quantity
        });
      }

      /* Add productList with its id to final response */
      products.push({
        id: response[i]._id,
        productList: productList
      });
    }

    return products;
  })
  .then(function (products) {
    /* This requests always succeeds, even if the CPF doesn't exists
     * it will simply return an empty list */
    res.status(200).send(products);
  })
})

/* POST add a new order */
router.post('/add', function(req, res, next) {
  var required = ['cpf', 'listaDeProdutos'];

  for (var i = 0; i < required.length; i++) {
    if (req.body[required[i]] === undefined) {
      res.status(400).send({
        message: "Dado não fornecido: " + required[i]
      });
      return;
    }
  }

  var cpf = req.body.cpf;
  var productList = req.body.listaDeProdutos;

  var order = new OrderModel({ cpf: cpf, productList: productList});
  order.save()
  .then(function(data) {
    return res.status(200).send({
      message: "Pedido registrado com sucesso."
    });
  })
  .catch(function(err) {
    res.status(500).send({
      message: err.message || "Erro no banco de dados interno."
    });
  });
});

module.exports = router;
