import React, { Component, Fragment } from 'react';
import { LinkContainer } from "react-router-bootstrap";
import { Button, Col, FormControl, FormGroup, Grid, Nav, Navbar, NavItem, Row } from "react-bootstrap";
import Routes from "./Routes";
import './App.css';
import img_carrinho from './carrinho.png';

class App extends Component {
  constructor(props) {
    super(props);

    var isAdmin = false;
    try {
      isAdmin = JSON.parse(localStorage.getItem("isAdmin"));
    } catch (e) { }

    this.state = {
      isAdmin: isAdmin,

      /* Authentication-related state variables */
      search:"",
      isAuthenticated: false,
      isAuthenticating: true
    };
  };

  async componentDidMount() {
    /* After rendering, verify if the user already logged on a previous
       session */
    this.state.isAuthenticated = false;
    var responseStatus;

    try {
      var loggedUser = localStorage.getItem("loggedUser");
      var sessionToken = localStorage.getItem("sessionToken");
      var userCpf = localStorage.getItem("userCpf");

      /* If there is a session token on browser storage, call an API to verify
         if it is equal to the one in database (if it differs, it means that the
         user logged on another browser/computer, and only one login is allowed
         at a time - for now) */
      if (loggedUser && sessionToken) {
        const request = {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            tokenSessao: sessionToken
          })
        };

        await fetch("http://localhost:3002/users/" + userCpf + "/logged", request)
        .then(function (response) {
          responseStatus = response.status;
          if (responseStatus !== 200 && responseStatus !== 401 && responseStatus !== 404) {
            alert("Sua sessão expirou! Por favor, faça login novamente.");
          }
        });
      }
    } catch(e) {
      alert(e);
    }

    if (responseStatus === 200) {
      this.state.isAuthenticated = true;
    }
    this.setState({ isAuthenticating: false });
  }

  /* Toggle isAuthenticated state variable */
  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated });
  }

  /* Toggle user Admin status */
  userIsAdmin = isAdmin => {
    this.setState({ isAdmin: isAdmin });
  }

  /* On logout, simply deleted session token stored on browser */
  handleLogout = event => {
    localStorage.removeItem("loggedUser");
    localStorage.removeItem("sessionToken");
    localStorage.removeItem("userCpf");
    localStorage.removeItem("isAdmin");
    this.userIsAdmin(false);
    this.userHasAuthenticated(false);
    window.location.reload();
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,
      userIsAdmin: this.userIsAdmin,
    }

    return (
      !this.state.isAuthenticating &&
      <div className="App">
        <Fragment>
          <div className="full-header">
            <Grid fluid className="header-wrapper">
              <Row>
                <Col sm={2} className="container-left-top-header">
                  <a href="/">
                    {this.state.isAdmin
                      ? <strong style={{'textAlign':'center','color':'#FFFFFF'}}>magazine giovana (administrador)</strong>
                      : <strong style={{'textAlign':'center','color':'#FFFFFF'}}>magazine giovana</strong> }
                  </a>
                </Col>
                <Col sm={7}>
                  <Row align="start" className="container-links-top-header">
                    <Col sm={12} className="red">
                      <a className="generic-links" href="contato">Contato</a>
                      <a className="generic-links" href="nossasLojas">Nossas lojas</a>
                    </Col>
                  </Row>
                  <Row align="start" className="container-search">
                    <Col sm={12}>
                    <form>
                      <FormGroup controlId = "search">
                        <FormControl 
                                    type="text"
                                    value={this.state.search}
                                    onChange={this.handleChange}
                                    placeholder="digite o nome do produto"
                                    bsSize="sm"
                                    style={{float:'left', width: '80%', height: '40px'}}/>
                        <Button type="submit" style={{float:'left', width: '20%', height: '40px', textAlign: 'center'}} href={"/buscarProduto/"+this.state.search} >Buscar</Button>
                      </FormGroup>
                      </form>
                    </Col>
                  </Row>
                </Col>
                <Col sm={2} className="container-login">
                  {this.state.isAuthenticated
                    ? <Fragment>
                        {this.state.isAdmin
                          ? <a className="generic-links" href="cadastrarProduto">Cadastrar Produto</a>
                          : <a className="generic-links" href="minhaConta">Minha Conta</a>}
                        {this.state.isAdmin
                          ? <a className="generic-links" href="logs">Logs</a>
                          : null}
                        <Button type="submit"
                                className="logout-button"
                                onClick={this.handleLogout}>Logout</Button>
                      </Fragment>
                    : <Fragment>
                        <a className="generic-links" href="login">Login</a>
                        <a className="generic-links" href="cadastrar">Cadastro</a>
                      </Fragment>}
                </Col>
                <Col sm={1} className="container-cart">
                    <a href="carrinho">
                      <img src={img_carrinho} alt="cart" width="40px"
                           height="40px" />
                    </a>
                </Col>
              </Row>
            </Grid>

            <div className="line-navbar">
              <Grid fluid className="container-navbar">
                <Row sm={12}>
                  <Navbar fluid collapseOnSelect>
                    <Navbar.Header>
                      <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                      <Nav pullRight>
                        <LinkContainer to="/filtro-ofertas">
                          <NavItem>Ofertas do dia</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/filtro-celular">
                          <NavItem>Celulares</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/filtro-moveis">
                          <NavItem>Móveis</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/filtro-eletrodomesticos">
                          <NavItem>Eletrodomésticos</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/filtro-informatica">
                          <NavItem>Informática</NavItem>
                        </LinkContainer>
                      </Nav>
                    </Navbar.Collapse>
                  </Navbar>
                </Row>
              </Grid>
            </div>
          </div>
          <Routes childProps={ childProps } />
        </Fragment>
      </div>
    );
  }
}

export default App;
