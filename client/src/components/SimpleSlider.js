import React from "react";
import Slider from "react-slick";
import './SimpleSlider.css';
import nvdia from './nvdia.jpg';
import cellphone from './cellphone.jpg'

export default class SimpleSlider extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div>
        <link rel="stylesheet" type="text/css" charSet="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <Slider {...settings}>
          <div>
            <img src={nvdia} alt="product1" />
          </div>
          <div>
            <img src={cellphone} alt="product2" />
          </div>
        </Slider>
      </div>
    );
  }
}
