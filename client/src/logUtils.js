export async function addLog (text, tag) {
  const date = new Date();
  const request = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      text: text,
      tag: tag,
      timestamp: date.getTime()
    })
  };
  await fetch('http://localhost:3001/log', request);
}

export async function getLogsByTag(tag) {
  var logs = null;
  const request = {
    method: "GET"
  };
  await fetch('http://localhost:3001/log/tag/' + tag, request)
  .then(async function (response) {
    logs = await response.json();
  });
  return logs;
}

export async function getAllLogs() {
  var logs = null;
  const request = {
    method: "GET"
  };
  await fetch('http://localhost:3001/log/all', request)
  .then(async function (response) {
    logs = await response.json();
  });
  return logs;
}
