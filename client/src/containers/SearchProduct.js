import React, { Component } from "react";
import { ControlLabel, FormGroup, FormControl, Col, Grid, Panel, Row, Button } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import "./SearchProduct.css";
import oven from '../fogao.png';

export default class SearchProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      text: '',
      searchResult: []
    };
    this.handleInit();
  }

  validateForm() {
    return (
      this.state.text.length > 0
    );
  }

  /* Handle when the search comes from App.js */
  handleInit = async event => {
    this.setState({ isLoading: true });
    if(this.props){
      this.state.text=this.props.match.params.nome;
      console.log(this.state.text);
    

      const searchBaseURL = 'http://ec2-18-218-218-216.us-east-2.compute.amazonaws.com:8080/api/products?page=0&itemsPerPage=100&name=';

      try {
        const response = await fetch(searchBaseURL + this.state.text, {
          method: 'GET',
          headers: {
            "Content-Type": "application/json",
            "Authorization": " Basic " +
                              new Buffer("cliente:kufvamzd").toString("base64")
          }
        });
        let searchResult = await response.json();
        this.setState({searchResult: searchResult['content'], isLoading: false});
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    const searchBaseURL = 'http://ec2-18-218-218-216.us-east-2.compute.amazonaws.com:8080/api/products?page=0&itemsPerPage=100&name=';

    try {
      const response = await fetch(searchBaseURL + this.state.text, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          "Authorization": " Basic " +
                            new Buffer("cliente:kufvamzd").toString("base64")
        }
      });
      let searchResult = await response.json();
      this.setState({searchResult: searchResult['content'], isLoading: false});
    } catch (e) {
      alert(e);
    }
  }

  /*if the search results are empty renders a message otherwise it renders the products related to the search*/
  renderSearchResult() {
    if(this.state.searchResult.length===0){
      return(
        <div>Não há produtos relacionados à pesquisa: {this.state.text}</div>
      )
    }else{
      return this.state.searchResult.map((item) => {
        return (
          <Col sm={3}>
            <Panel>
              <Panel.Heading>
                {item.name}
              </Panel.Heading>
              <Panel.Body>
                <Row>
                  <img src={oven} alt="placeholder" className="image-wrapper"/>
                </Row>
                <Row>
                  {item.description}
                </Row>
                <Row>
                  {item.value}R$
                </Row>
                <Button onClick={() => this.addCart(item)}>Adicionar ao Carrinho</Button>
              </Panel.Body>
            </Panel>
          </Col>
        )
      })
    }
  }

  render() {
    return (
      <div>
        <div className="SearchProduct">
          <form onSubmit={this.handleSubmit}>

            <FormGroup controlId="text" bsSize="large">
              <ControlLabel>Produto</ControlLabel>
              <FormControl
                autoFocus
                type="text"
                value={this.state.text}
                onChange={this.handleChange}
              />
            </FormGroup>

            <LoaderButton
              block
              bsSize="large"
              disabled={!this.validateForm()}
              type="submit"
              isLoading={this.state.isLoading}
              text="Buscar"
              loadingText="Searching…"
            />
          </form>
        </div>
        <Grid fluid className="products-wrapper">
          { this.renderSearchResult() }
        </Grid>
      </div>
    );
  }
}
