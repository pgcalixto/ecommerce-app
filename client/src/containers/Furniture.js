import React, { Component, Fragment } from "react";
import { Col, Grid, Panel, Row, Button } from "react-bootstrap";
import "./Furniture.css";
import placeholder from '../placeholder.jpg';

export default class Furniture extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      searchResult: []
    };

    this.handleInit();
  }

  handleInit = async event => {
    this.setState({ isLoading: true });

    const searchBaseURL = 'http://ec2-18-218-218-216.us-east-2.compute.amazonaws.com:8080/api/products?page=0&itemsPerPage=100&category=MOVEIS';

    try {
      const response = await fetch(searchBaseURL, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json",
          "Authorization": " Basic " +
                            new Buffer("cliente:kufvamzd").toString("base64")
        }
      });
      let searchResult = await response.json();
      this.setState({searchResult: searchResult['content'], isLoading: false});
    } catch (e) {
      console.log(e);
    }
  }

  renderSearchResult() {
    if(this.state.searchResult.length===0){
      return(
        <div>Não há móveis disponíveis em estoque!</div>
      )
    }else{
      return this.state.searchResult.map((item) => {
        return (
          <Col sm={3}>
            <Panel>
              <Panel.Heading>
                <b>{item.name}</b>
              </Panel.Heading>
              <Panel.Body>
                <Row>
                  {item.images.length > 0
                  ? <img src={item.images[0].url} alt="item.name"
                      className="image-wrapper"/>
                  : <img src={placeholder} alt="placeholder"
                      className="image-wrapper"/>}
                </Row>
                <Row>
                  {item.description}
                </Row>
                <Row>
                  R$ {item.value}
                </Row>
                <Button onClick={() => this.addCart(item)}>Adicionar ao Carrinho</Button>
              </Panel.Body>
            </Panel>
          </Col>
        )
      })
    }
  }

  async addCart(item) {
    let cart = JSON.parse(await localStorage.getItem("cart"));

    if (cart === null) {
      cart = {};
    }
    // cart doesn't contain item: add it to cart
    if (cart[item.id] === undefined) {
      cart[item.id] = {
        item: item,
        count: 1
      };
    }
    // cart already has item: increment item count
    else {
      cart[item.id].count = cart[item.id].count + 1;
    }

    await localStorage.setItem("cart", JSON.stringify(cart));
  }

  render() {
    return (
      <Fragment>
        <div className="Furniture">
        <h2>Móveis</h2>
        </div>
        <Grid fluid className="products-wrapper">
          { this.renderSearchResult() }
        </Grid>
      </Fragment>
    );
  }
}
