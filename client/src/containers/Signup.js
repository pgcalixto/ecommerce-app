import React, { Component } from "react";
import { HelpBlock, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { addLog } from '../logUtils';
import "./Signup.css";

export default class Signup extends Component {
  constructor(props) {
    super(props);

    var confirmedUser = localStorage.getItem("confirmedUser");
    try {
      confirmedUser = JSON.parse(confirmedUser);
      confirmedUser = confirmedUser != null ? confirmedUser : true;
    } catch(e) {
      confirmedUser = true;
    }

    if (confirmedUser === true) {
      localStorage.removeItem("confirmedUser");
    }

    this.state = {
      isLoading: false,
      email: "",
      cpf: "",
      password: "",
      confirmPassword: "",
      name: "",
      dateOfBirth: "",
      phone: "",
      confirmationCode: "",
      newUser: confirmedUser
    };
  }

  validateForm() {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword &&
      this.state.name.length > 0 &&
      this.state.dateOfBirth.length > 0 &&
      this.state.phone.length > 0
    );
  }

  validateConfirmationForm() {
    return this.state.confirmationCode.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    var responseStatus;
    try {
      let randomScore = 400 + parseInt((Math.random() * 200), 10);
      let body = encodeURIComponent("cpf") + "=" + encodeURIComponent(this.state.cpf)
                 + "&" +
                 encodeURIComponent("score") + "=" + encodeURIComponent(randomScore);
      const creditRequest = {
        method: 'PUT',
        body: body,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charSet=UTF-8"
        }
      }

      await fetch("http://ec2-54-233-234-42.sa-east-1.compute.amazonaws.com:3000/api/v1/inserir",
      creditRequest)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
    } catch (e) {
      alert(e);
    }

    var responseStatus2;
    if (responseStatus === 201) {
      try {
        const registerRequest = {
          method: 'POST',
          body: JSON.stringify({
            cpf: this.state.cpf,
            email: this.state.email,
            senha: this.state.password,
            nome: this.state.name,
            dataDeNascimento: this.state.dateOfBirth,
            telefone: this.state.phone
          }),
          headers: {
            "Content-Type": "application/json"
          }
        }

        await fetch('http://localhost:3001/users/add', registerRequest)
        .then(function (response) {
          responseStatus2 = response.status;
          return response.json();
        })
        .then(function (data) {
          console.log(data);
          if (responseStatus2 !== 200) {
            addLog('Erro no cadastro de ' + this.state.email,
                   'cadastroUsuario');
            alert("Erro no cadastro: " + data.message);
          }
        });
      } catch (e) {
        alert(e);
      }
    }

    if (responseStatus2 === 200) {
      addLog('Usuário ' + this.state.email + ' cadastrado', 'cadastroUsuario');
      alert("Sucesso no cadastro!");
      this.setState({ newUser: false });
    }
    this.setState({ isLoading: false });
  }

  handleConfirmationSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    var responseStatus;
    try {
      const request = {
        method: 'POST',
        body: JSON.stringify({
          registerToken: this.state.confirmationCode
        }),
        headers: {
          "Content-Type": "application/json"
        }
      }

      await fetch('http://localhost:3002/confirm', request)
      .then(function (response) {
        responseStatus = response.status;
        return response.json()
      })
      .then(function (data) {
        alert(data.message);
      })
    } catch (e) {
      alert(e);
    }

    if (responseStatus === 200) {
      this.props.history.push("/login");
    }
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmationSubmit}>
        <FormGroup controlId="confirmationCode" bsSize="large">
          <ControlLabel>Confirmation Code</ControlLabel>
          <FormControl
            autoFocus
            type="tel"
            value={this.state.confirmationCode}
            onChange={this.handleChange}
          />
          <HelpBlock>Please check your email for the code.</HelpBlock>
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateConfirmationForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Verificar"
          loadingText="Verificando..."
        />
      </form>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="cpf" bsSize="large">
          <ControlLabel>CPF</ControlLabel>
          <FormControl
            type="text"
            value={this.state.cpf}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Senha</ControlLabel>
          <FormControl
            value={this.state.password}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirmar senha</ControlLabel>
          <FormControl
            value={this.state.confirmPassword}
            onChange={this.handleChange}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="name" bsSize="large">
          <ControlLabel>Nome</ControlLabel>
          <FormControl
            value={this.state.name}
            onChange={this.handleChange}
            type="text"
          />
        </FormGroup>
        <FormGroup controlId="dateOfBirth" bsSize="large">
          <ControlLabel>Data de nascimento</ControlLabel>
          <FormControl
            value={this.state.dateOfBirth}
            onChange={this.handleChange}
            type="text"
          />
        </FormGroup>
        <FormGroup controlId="phone" bsSize="large">
          <ControlLabel>Telefone</ControlLabel>
          <FormControl
            value={this.state.phone}
            onChange={this.handleChange}
            type="text"
          />
        </FormGroup>
        <LoaderButton
          block
          bsSize="large"
          disabled={!this.validateForm()}
          type="submit"
          isLoading={this.state.isLoading}
          text="Cadastrar"
          loadingText="Cadastrando..."
        />
      </form>
    );
  }

  render() {
    return (
      <div className="Signup">
        {this.state.newUser === true
          ? this.renderForm()
          : this.renderConfirmationForm()}
      </div>
    );
  }
}
