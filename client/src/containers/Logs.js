import React, { Component, Fragment } from "react";
import { getLogsByTag, getAllLogs } from '../logUtils';

export default class Logs extends Component {
  constructor(props) {
    super(props);
    this.showLogs = this.showLogs.bind(this);
  }

  writeToTextArea(logs) {
    if (logs === undefined || logs === null) {
      return;
    }

    // get text area element
    let textArea = document.getElementById('logDisplay');

    // initialize text area element
    textArea.value = "";

    // for each log in response body: add log to text area
    for(let log of logs) {
      let date = new Date(log.timestamp);
      let dateString = ("0000" + date.getFullYear()).slice(-4) + "/" +
                       ("0" + date.getMonth()).slice(-2) + "/" +
                       ("0" + date.getDate()).slice(-2) + " " +
                       ("0" + date.getHours()).slice(-2) + ":" +
                       ("0" + date.getMinutes()).slice(-2) + ":" +
                       ("0" + date.getSeconds()).slice(-2);
      textArea.value = textArea.value + dateString + ": " + log.text + '\n';
    }
  }

  async showLogs(event) {
    if (event.target.value === "" || event.target.value == undefined) {
      return;
    }

    var logs = null;
    if(event.target.value === 'all') {
      logs = await getAllLogs();
    } else {
      logs = await getLogsByTag(event.target.value);
    }
    // write logs to text area
    this.writeToTextArea(logs);
  }

  render() {
    return (
      <Fragment>
        <select style={{"vertical-align": "top"}} onChange={this.showLogs}>
        <option value="">Selecione uma categoria</option>
          <option value="cadastroUsuario">Cadastro de usuário</option>
          <option value="login">Login</option>
          <option value="registroProduto">Registro de produtos</option>
          <option value="minhaconta">Minha Conta</option>
          <option value="pagamento">Pagamentos</option>
          <option value="all">Todos os logs</option>
        </select>
        <textarea id="logDisplay" rows="20" cols="70">
        </textarea>
      </Fragment>
    );
  }
}
