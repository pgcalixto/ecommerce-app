import React, { Component, Fragment } from "react";
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup,
         MenuItem, Panel }
       from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import MaskedFormControl from 'react-bootstrap-maskedinput'
import { addLog } from '../logUtils';
import "./Payment.css";

const debitCard = 0;
const bankSlip = 1;
const creditCard = 2;

const bancoDoBrasilKey = "Banco do Brasil";
const caixaKey = "Caixa Economica Federal";
const itauKey = "Itau";
const hsbcKey = "HSBC";

export default class Payment extends Component {
  constructor(props) {
    super(props);

    let cpf = localStorage.getItem("userCpf");
    let formattedCpf = "";
    if (cpf != null) {
      formattedCpf = cpf.slice(0, 3) + "." + cpf.slice(3, 6) + "." + cpf.slice(6, 9) + "-" + cpf.slice(9);
    } else {
      alert("Você não está logado! Por favor, faça login para continuar a compra.");
    }

    let cart = JSON.parse(localStorage.getItem("renderableCart"));

    let purchaseValue = localStorage.getItem("purchaseValue");
    let paymentOption = localStorage.getItem("paymentOption");
    var installmentsAmount = -1;
    if (paymentOption == creditCard) {
      installmentsAmount = localStorage.getItem("installmentsAmount");
    }

    this.state = {
      cpf: cpf,
      cart: cart,
      formattedCpf: formattedCpf,
      purchaseValue: purchaseValue,
      paymentOption: paymentOption,
      installmentsAmount: installmentsAmount,

      paymentSubmitionLoading: false,

      cardName: "",
      cardNumber: "",
      verifyingCode: "",
      expirationDate: "",

      selectedBank: ""
    }
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  validatePaymentData() {
    if (this.state.paymentOption == bankSlip) {
      return this.state.selectedBank.length > 0
    } else {
      for (let i = 0; i < this.state.cardNumber.length; i++) {
        if (this.state.cardNumber[i] === '_') {
          return false;
        }
      }

      for (let i = 0; i < this.state.verifyingCode.length; i++) {
        if (this.state.verifyingCode[i] === '_') {
          return false;
        }
      }

      for (let i = 0; i < this.state.expirationDate.length; i++) {
        if (this.state.expirationDate[i] === '_') {
          return false;
        }
      }

      return (
        this.state.cardName.length > 0
        && this.state.cardNumber.length > 0
        && this.state.verifyingCode.length > 0
        && this.state.expirationDate.length > 0
      )
    }
  }

  async addOrderToInternalDb(productList) {
    var resumedList = []
    for (let i = 0; i < productList.length; i++) {
      resumedList.push({
        productId: productList[i].id,
        quantity: productList[i].quantity
      })
    }

    const request = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        cpf: this.state.cpf,
        listaDeProdutos: resumedList
      })
    }

    var responseStatus;
    await fetch("http://localhost:3001/orders/add", request)
    .then(function (response) {
      responseStatus = response.status;
      return response.json();
    })
    .then(function (data) {
      if (responseStatus !== 200) {
        alert("Erro no banco de dados interno. " + data.message);
      }
    })
  }

  handleSubmit = async event => {
    event.preventDefault();

    let requestUrl;
    let body;

    let today = new Date();
    let dd = today.getDate(); dd = dd < 10 ? '0' + dd : dd;
    let mm = today.getMonth() + 1; mm = mm < 10 ? '0' + mm : mm;
    let todayString = dd + '/' + mm + '/' + today.getFullYear();

    let cardNumberString = this.state.cardNumber.split(" ").join("");

    if (this.state.paymentOption == bankSlip) {
      let slipExpirationDate = today; slipExpirationDate.setDate(slipExpirationDate.getDate() + 3);
      let dd = slipExpirationDate.getDate(); dd = dd < 10 ? '0' + dd : dd;
      let mm = slipExpirationDate.getMonth() + 1; mm = mm < 10 ? '0' + mm : mm;
      let slipExpirationDateString = dd + '/' + mm + '/' + slipExpirationDate.getFullYear();

      requestUrl = "http://mc851-pagamento.qieckpkezf.sa-east-1.elasticbeanstalk.com/servico/pagamento_boleto";
      body = JSON.stringify({
        cpf_comprador: this.state.cpf,
        valor_compra: this.state.purchaseValue,
        cnpj_site: "62429463446446",
        data_emissao_pedido: todayString,
        banco_gerador_boleto: this.state.selectedBank,
        data_vencimento_boleto: slipExpirationDateString,
        endereco_fisico_site: "Av. Albert Einstein, 400 - Distrito Barão Geraldo, Campinas - SP, 13083-852"
      });
    } else {
      requestUrl = "http://mc851-pagamento.qieckpkezf.sa-east-1.elasticbeanstalk.com/servico/pagamento_cartao";
      body = JSON.stringify({
        cpf_comprador: this.state.cpf,
        valor_compra: this.state.purchaseValue,
        cnpj_site: "62429463446446",
        data_emissao_pedido: todayString,
        numero_cartao: cardNumberString,
        nome_cartao: this.state.cardName,
        cvv_cartao: this.state.verifyingCode,
        data_vencimento_cartao: this.state.expirationDate,
        credito: this.state.paymentOption == debitCard ? "0" : "1",
        num_parcelas: this.state.paymentOption == creditCard ? this.state.installmentsAmount : 1
      });
    }

    const request = {
      method: "POST",
      header: {
        "Content-Type": "application/json"
      },
      body: body
    }

    var resultado;
    try {
      await fetch(requestUrl, request)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        resultado = data;
      })
    } catch(e) {
      addLog('Erro ao realizar pagamento de usuário ' + this.state.cpf, 'pagamento');
      alert("Erro ao realizar o pagamento, por favor tente novamente!");
    }

    if (this.state.paymentOption == bankSlip) {
      if (resultado.status === true) {
        let slipNumber = resultado.num_boleto;
        alert("Boleto gerado! Número: " + slipNumber);
        this.addOrderToInternalDb(this.state.cart);
        window.location.reload();
        localStorage.removeItem("cart");
      } else {
        alert("Erro ao gerar o boleto! Por favor, verifique os dados submetidos e tente novamente."
              + "Se o erro persistir, entre em contato com o seu banco.");
      }
    } else {
      if (resultado.pagamento == 1) {
        addLog('Pagamento de usuário ' + this.state.cpf + ' concluído', 'pagamento');
        alert("Pagamento concluído!");
        this.addOrderToInternalDb(this.state.cart);
        window.location.reload();
        localStorage.removeItem("cart");
      } else {
        if (resultado.pagamento == -1) {
          addLog('Pagamento de usuário ' + this.state.cpf + ' não autorizado', 'pagamento');
          alert("Pagamento não aprovado pelo banco.");
        } else {
          addLog('Erro ao realizar pagamento de usuário ' + this.state.cpf, 'pagamento');
          alert("Erro ao realizar o pagamento, por favor verifique os dados submetidos e tente novamente!");
        }
      }
    }
  }

  renderCardPayment() {
    return (
      <div>
        <FormGroup controlId="cardName">
          <Col componentClass={ControlLabel} sm={3} smOffset={2}>
            Nome (como no cartão):
          </Col>
          <Col sm={5}>
            <FormControl
              type="text"
              value={this.state.cardName}
              onChange={this.handleChange}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="cardNumber">
          <Col componentClass={ControlLabel} sm={3} smOffset={2}>
            Número do cartão:
          </Col>
          <Col sm={4}>
            <MaskedFormControl
              type="text"
              value={this.state.cardNumber}
              onChange={this.handleChange}
              mask='1111 1111 1111 1111'
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="verifyingCode">
          <Col componentClass={ControlLabel} sm={1} smOffset={4}>
            CVV:
          </Col>
          <Col sm={2}>
            <MaskedFormControl
              type="text"
              value={this.state.verifyingCode}
              onChange={this.handleChange}
              mask='111'
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="expirationDate">
          <Col componentClass={ControlLabel} sm={3} smOffset={2}>
            Data de vencimento:
          </Col>
          <Col sm={3}>
            <MaskedFormControl
              type="text"
              value={this.state.expirationDate}
              onChange={this.handleChange}
              mask='11/11/1111'
            />
          </Col>
        </FormGroup>
      </div>
    );
  }

  renderBankSlipPayment() {
    const onSelectBank = eventKey => {
      this.setState({
        selectedBank: eventKey,
      })
    }

    return (
      <FormGroup controlId="selectedBank">
        <DropdownButton title={this.state.selectedBank} id="selectedBank">
          <MenuItem eventKey={bancoDoBrasilKey} onSelect={onSelectBank}>
            {bancoDoBrasilKey}
          </MenuItem>
          <MenuItem eventKey={caixaKey} onSelect={onSelectBank}>
            {caixaKey}
          </MenuItem>
          <MenuItem eventKey={itauKey} onSelect={onSelectBank}>
            {itauKey}
          </MenuItem>
          <MenuItem eventKey={hsbcKey} onSelect={onSelectBank}>
            {hsbcKey}
          </MenuItem>
        </DropdownButton>
      </FormGroup>
    );
  }

  render() {
    return (
      <div className="Payment">
        {this.state.cpf
        ? <Fragment>
            <div className="Payment">
              <h3>
                Finalize sua compra!
              </h3>
            </div>

            <Form horizontal onSubmit={this.handleSubmit} className="formWrapper">
              <Panel>
                <Panel.Body>
                  <FormGroup controlId="customerCpf">
                    <Col componentClass={ControlLabel} sm={2} smOffset={3}>
                      CPF:
                    </Col>
                    <Col sm={3}>
                      <FormControl
                        type="text"
                        value={this.state.formattedCpf}
                        readOnly={true}
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup controlId="purchaseValue">
                    <Col componentClass={ControlLabel} sm={3} smOffset={2}>
                      Valor da compra:
                    </Col>
                    <Col sm={3}>
                      <FormControl
                        type="text"
                        value={"R$ " + this.state.purchaseValue}
                        readOnly={true}
                      />
                    </Col>
                  </FormGroup>

                  <h4 className="paymentData">
                    Dados de Pagamento
                  </h4>

                  {this.state.paymentOption == bankSlip
                  ? this.renderBankSlipPayment()
                  : this.renderCardPayment()}

                  <Col sm={3} smOffset={3}>
                    <LoaderButton
                      block
                      bsStyle="success"
                      disabled={!this.validatePaymentData()}
                      type="submit"
                      isLoading={this.state.paymentSubmitionLoading}
                      text="Concluir"
                      loadingText="Submetendo..."
                    />
                  </Col>
                  <Col sm={3}>
                    <Button
                      block
                      bsStyle="danger"
                      href="carrinho"
                    >
                      Cancelar
                    </Button>
                  </Col>
                </Panel.Body>
              </Panel>
            </Form>
          </Fragment>
        : <div>
            Você não está logado! Por favor, faça login para comprar.
          </div>}
      </div>
    )
  }
}
