import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { addLog } from '../logUtils';
import "./ProductRegister.css";

export default class ProductRegister extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      name: "",
      type: "",
      category: "",
      quantityInStock: "" ,
      value: "",
      availableToSell: false,
      weight: "",
      description: ""
    };
  }

  validateForm() {
    return (
      this.state.name.length > 0 &&
      this.state.type.length > 0 &&
      this.state.category.length > 0 &&
      this.state.quantityInStock.length > 0 &&
      this.state.value.length > 0 &&
      this.state.weight.length > 0 &&
      this.state.description.length > 0
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    try {
      const response = await fetch('http://ec2-18-218-218-216.us-east-2.compute.amazonaws.com:8080/api/products', {
        method: 'POST',
        body: JSON.stringify({
          name: this.state.name,
          ownerGroup: "GioGio",
          category: this.state.category,
          type: this.state.type,
          quantityInStock: this.state.quantityInStock,
          value: this.state.value,
          availableToSell: true,
          weight: this.state.weight,
          description: this.state.description
        }),
        headers: {
          "Content-Type": "application/json",
          "Authorization": " Basic " +
                            new Buffer("cliente:kufvamzd").toString("base64")
        }
      });
      console.log(response);
      if(response.status === 201) {
        addLog('Produto ' + this.state.name + ' cadastrado com sucesso',
               'registroProduto');
        alert("Produto Cadastrado com Sucesso!");
      }
      this.setState({ isLoading: false });
    } catch (e) {
      addLog('Erro ao cadastrar produto ' + this.state.name, 'registroProduto');
      alert(e);
    }
  }

  render() {
    return (
      <div className="ProductRegister">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="name" bsSize="large">
            <ControlLabel>Produto</ControlLabel>
            <FormControl
              autoFocus
              type="name"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="type" bsSize="large">
            <ControlLabel>Tipo</ControlLabel>
            <FormControl
              type="text"
              value={this.state.type}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="category" bsSize="large">
            <ControlLabel>Categoria</ControlLabel>
            <FormControl
              type="text"
              value={this.state.category}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="quantityInStock" bsSize="large">
            <ControlLabel>Quantidade em Estoque</ControlLabel>
            <FormControl
              type="text"
              placeholder=" "
              value={this.state.quantityInStock}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="value" bsSize="large">
            <ControlLabel>Valor</ControlLabel>
            <FormControl
              placeholder=" "
              type="text"
              value={this.state.value}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="weight" bsSize="large">
            <ControlLabel>Peso</ControlLabel>
            <FormControl
              value={this.state.weight}
              onChange={this.handleChange}
              type="text"
            />
          </FormGroup>
          <FormGroup controlId="description" bsSize="large">
            <ControlLabel>Descrição</ControlLabel>
            <FormControl
              value={this.state.description}
              onChange={this.handleChange}
              type="text"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            type="submit"
            isLoading={this.state.isLoading}
            text="Cadastrar"
            loadingText="Cadastrando Produto..."
          />
        </form>
      </div>
    );
  }
}
