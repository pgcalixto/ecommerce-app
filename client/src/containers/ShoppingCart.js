import React, { Component, Fragment } from "react";
import { Col, Grid, Label, Table, ToggleButton, ToggleButtonGroup, FormGroup,
         ControlLabel, DropdownButton,
         MenuItem } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import MaskedFormControl from 'react-bootstrap-maskedinput'
import "./ShoppingCart.css";
import oven from '../fogao.png';
import minus from '../minus.png';
import plus from '../plus.png';

const sedexKey = 0;
const pacKey = 1;

const debitCard = 0;
const bankSlip = 1;
const creditCard = 2;

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);

    const cpf = localStorage.getItem("userCpf");

    this.state = {
      isLoading: false,
      cpf: cpf,
      isLoadingCep: false,
      cep: '',
      cartProducts: {},
      renderableItems: [],
      cartTotal: 0,
      shippingSelectTitle: 'Método de envio',
      shippingSelected: false,
      showShipping: false,
      sedexPrice: 0,
      sedexTime: 0,
      pacPrice: 0,
      pacTime: 0,
      cartTotalWithShipping: 0,
      totalItems: 0,
      paymentOption: debitCard,
      installmentsAmount: 1
    };

    this.handleInit();
  }

  /* Shipping-related methods */
  /**
   * Function to validate if the CEP contains the correct 9 characters, so
   * that the option to calculate shipping costs is enabled.
   */
  validateForm() {
    return this.state.cep.length === 9;
  }

  handleCepChange = event => {
    this.setState({
      cep: event.target.value
    });
  }

  /**
   * When CEP is submitted, this function obtains shipping costs and updates the
   * state with the obtained data.
   */
  handleSubmitCep = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    // communicate with the shipping tax API
    let plainCep = this.state.cep.split("-").join("");
    let cepData = null;
    let formData = encodeURIComponent('CEP') + '=' +
                   encodeURIComponent(plainCep);

    const request = {
      method: 'POST',
      body: formData,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }

    await fetch('https://shielded-caverns-17296.herokuapp.com/frete/', request)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      cepData = data;
    });

    // update state with retrieved data
    if (cepData !== null && cepData.answer !== "ok") {
      alert("CEP inválido!");
      this.setState({isLoading: false});
      return;
    }

    this.setState({
      sedexPrice: cepData.sedexPrice,
      sedexTime: cepData.sedexTime,
      pacPrice: cepData.pacPrice,
      pacTime: cepData.pacTime,
      showShipping: true,
      isLoading: false
    });
  }

  /**
   * Take a number and return a string representing it a number of decimal
   * places.
   */
  toFixedNumber(num, fixed) {
    const re = new RegExp('^-?\\d+(?:\.\\d{0,' + (fixed || -1) + '})?');
    return num.toString().match(re)[0];
  }

  /**
   * Compute shopping cart totals and return results.
   */
  computeTotals(cartProducts) {
    let renderableItems = [];
    let cartTotal = 0;
    let totalItems = 0;

    // for each product id in the cart: add to totals
    for (let id in cartProducts) {

      // filter parent class properties
      if (cartProducts.hasOwnProperty(id)) {

        // get product attributes
        const name = cartProducts[id].item.name;
        const cost = cartProducts[id].item.value;
        const quantity = cartProducts[id].count;

        // compute cart totals
        cartTotal += cost * quantity;
        totalItems += quantity;

        // add product to renderable items list
        renderableItems.push({
          id: id,
          name: name,
          cost: cost,
          quantity: quantity
        });
      }
    }
    return {renderableItems: renderableItems,
      cartTotal: this.toFixedNumber(cartTotal, 2),
      totalItems: totalItems
    };
  }

  /**
   * Get cart from storage, compute totals and set initial component state.
   */
  handleInit = async event => {
    this.setState({ isLoading: true });
    let cartProducts = JSON.parse(await localStorage.getItem("cart"));
    let returnValue = this.computeTotals(cartProducts);

    // obtainment of user score
    var responseStatus;
    var userScore;
    if (this.state.cpf) {
      try {
        await fetch("http://ec2-54-233-234-42.sa-east-1.compute.amazonaws.com:3000/api/v1/score/" + this.state.cpf, {
          method: "GET"
        })
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          responseStatus = data.status;
          if (responseStatus === 200) {
            userScore = data.score;
          }
        })
      } catch (e) {
        alert(e);
      }
    } else {
      alert("Você não está logado! Por favor, faça login para continuar a compra.");
    }

    this.setState({
      isLoading: false,
      userScore: userScore,
      cartProducts: cartProducts,
      renderableItems: returnValue.renderableItems,
      cartTotal: returnValue.cartTotal,
      totalItems: returnValue.totalItems
    });
  }

  /**
   * Update the cart state after a change in cart products.
   */
  updateState(cartProducts) {
    let returnValue = this.computeTotals(cartProducts);
    this.setState({
      cartProducts: cartProducts,
      renderableItems: returnValue.renderableItems,
      cartTotal: returnValue.cartTotal,
      totalItems: returnValue.totalItems
    });
    localStorage.setItem("cart", JSON.stringify(cartProducts));
    localStorage.setItem("renderableCart", JSON.stringify(returnValue.renderableItems));
  }

  /**
   * Handle cart item add event.
   */
  handleDecrement(item)  {
    let cartProducts = this.state.cartProducts;
    let count = cartProducts[item.id].count;
    let remove = true;
    if (count === 1) {
      remove = window.confirm("Deseja remover o item do carrinho?");
    }
    if (remove === true) {
      cartProducts[item.id].count = count - 1;
    }
    this.updateState(cartProducts);
  }

  /**
   * Handle cart item remove event.
   */
  handleIncrement(item) {
    let cartProducts = this.state.cartProducts;
    let count = cartProducts[item.id].count;
    cartProducts[item.id].count = count + 1;
    this.updateState(cartProducts);
  }

  renderSearchResult() {
    const cartProducts = this.state.cartProducts;

    return this.state.renderableItems.map((item) => {
      if (cartProducts[item.id].count > 0) {
        return (
          <tr key={item.id}>
            <td>
              <img src={oven} alt="placeholder" className="image-list-wrapper"/>
            </td>
            <td>{item.name}</td>
            <td>
              <button onClick={this.handleDecrement.bind(this, item)}
              style={{"background":"none","border":"none","float":"left"}}>
                <img alt="minus" src={minus} className="control-buttons"/>
              </button>
              {item.quantity}
              <button onClick={this.handleIncrement.bind(this, item)}
              style={{"background":"none","border":"none","float":"right"}}>
                <img alt="plus" src={plus} className="control-buttons"/>
              </button>
            </td>
            <td>R$ {item.cost}</td>
          </tr>
        )
      }
    })
  }

  togglePaymentOption(status) {
    this.setState({
      paymentOption: status
    });
  }

  modifyInstallmentsAmount(score, factor) {
    let value = this.state.installmentsAmount;

    if (factor < 0) {
      if (value > 1) {
        value = value - 1;
      }
    } else {
      let max;
      if (score > 700) {
        max = 12;
      } else if (score > 600) {
        max = 7;
      } else {
        max = 3;
      }

      if (value < max) {
        value = value + 1;
      }
    }

    this.setState({
      installmentsAmount: value
    })
  }

  handlePaymentSubmition() {
    if (this.state.cartTotalWithShipping === 0) {
      alert("Por favor, selecione um método de envio!");
    } else {
      localStorage.setItem("purchaseValue", this.state.cartTotalWithShipping);
      localStorage.setItem("paymentOption", this.state.paymentOption);
      if (this.state.paymentOption == creditCard) {
        localStorage.setItem("installmentsAmount", this.state.installmentsAmount);
      }
      this.props.history.push("/pagamento");
    }
  }

  /**
   * Render the shipping options after CEP is validated, showing SEDEX and PAC
   * prices and delivery times.
   */
  renderShippingOptions() {

    // When user selects a shipping method, update the total cost (including
    // shipping) and the dropdown menu title.
    const onSelectShipping = eventKey => {
      let cartTotalWithShipping = parseFloat(this.state.cartTotal);
      let shippingSelectTitle = '';

      if (eventKey === sedexKey) {
        cartTotalWithShipping += parseFloat(this.state.sedexPrice);
        shippingSelectTitle = 'SEDEX';
      } else if (eventKey === pacKey) {
        cartTotalWithShipping += parseFloat(this.state.pacPrice);
        shippingSelectTitle = 'PAC';
      }

      this.setState({
        cartTotalWithShipping: cartTotalWithShipping.toString(),
        shippingSelectTitle: shippingSelectTitle,
        shippingSelected: true
      });
    };

    return (
      <div style={{"paddingTop": "16px"}}>
        {this.state.showShipping
        ? <DropdownButton
            title = {this.state.shippingSelectTitle}
          >
            <MenuItem eventKey={sedexKey} onSelect={onSelectShipping}>
              SEDEX: R$ {this.state.sedexPrice}, {this.state.sedexTime} dias
            </MenuItem>
            <MenuItem eventKey={pacKey} onSelect={onSelectShipping}>
              PAC: R$ {this.state.pacPrice}, {this.state.pacTime} dias
            </MenuItem>
          </DropdownButton>
        : null}
      </div>
    );
  }

  /**
   * Render the total price, include shipping, after shipping method is
   * selected.
   */
  renderCartWithShipping() {
    return (
      <div>
        {this.state.shippingSelected
        ? <Grid fluid className="products-wrapper">
            <Table striped bordered condensed hover>
              <tbody>
                <tr>
                  <td><b>Total + frete</b></td>
                  <td>R$ {this.state.cartTotalWithShipping}</td>
                </tr>
              </tbody>
            </Table>
          </Grid>
        : null}
      </div>
    );
  }

  renderPaymentOptions() {
    const score = this.state.userScore;

    return (
      <div style={{"paddingTop": "30px"}}>
        <ToggleButtonGroup vertical type="radio" name="paymentOptions"
         defaultValue={this.state.paymentOption !== creditCard ? debitCard : creditCard}>
          {score
          ? <ToggleButton value={debitCard} className="payment-option"
              onClick={() => this.togglePaymentOption(debitCard)}>
              Cartão de Débito
            </ToggleButton>
          : <div>
              Algo de errado aconteceu x( tente novamente mais tarde!
            </div>}
          {score > 400
          ? <ToggleButton value={bankSlip} className="payment-option"
             onClick={() => this.togglePaymentOption(bankSlip)}>
               Boleto Bancário
            </ToggleButton>
          : null}
          {score > 500
          ? <ToggleButton value={creditCard} className="payment-option"
             onClick={() => this.togglePaymentOption(creditCard)}>
              Cartão de Crédito
            </ToggleButton>
          : null}
          {this.state.paymentOption === creditCard
          ? <div style={{"marginBottom": "8px"}}>
              Parcelas:
              <img alt="minus" src={minus}
               onClick={() => this.modifyInstallmentsAmount(score, -1)}
               className="control-buttons"/>
              <Label bsStyle="primary" style={{"margin": "0px 8px"}}>
                {this.state.installmentsAmount}
              </Label>
              <img alt="plus" src={plus}
               onClick={() => this.modifyInstallmentsAmount(score, +1)}
               className="control-buttons"/>
            </div>
          : null}
        </ToggleButtonGroup>
        {score
        ? <LoaderButton
            block
            bsStyle="success"
            type="submit"
            isLoading={this.state.isLoading}
            onClick={() => this.handlePaymentSubmition()}
            text="Continuar"
            loadingText="Carregando..."
            style={{"width": "120px", "margin": "0px auto 30px auto"}}
          />
        : null}
      </div>
    )
  }

  render() {
    return (
      <Fragment>
        <div className="ShoppingCart">
          <h3>Meu Carrinho</h3>
        </div>
        <Grid fluid className="products-wrapper">
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Img</th>
                <th>Produto</th>
                <th>Quantidade</th>
                <th>Valor Unidade</th>
              </tr>
            </thead>
            <tbody>
              { this.renderSearchResult() }
              <tr>
                <td><b>Totais do carrinho</b></td>
                <td></td>
                <td>{this.state.totalItems}</td>
                <td>R$ {this.state.cartTotal}</td>
              </tr>
            </tbody>
          </Table>
        </Grid>

        <Grid fluid className="shipment-options-wrapper">
          <Col sm={4} smOffset={4}>
            <form onSubmit={this.handleSubmitCep}>
              <FormGroup controlId="cep">
                <ControlLabel>CEP de entrega</ControlLabel>
                <div className="centerChildren">
                  <MaskedFormControl
                    type='text'
                    value={this.state.cep}
                    onChange={this.handleCepChange}
                    style={{"width": "50%"}}
                    mask='11111-111'
                  />
                </div>
              </FormGroup>
              <div className="centerChildren">
                <LoaderButton
                  block
                  bsSize="small"
                  style={{"width": "50%"}}
                  disabled={!this.validateForm()}
                  type="submit"
                  isLoading={this.state.isLoadingCep}
                  text="Calcular frete"
                  loadingText="Calculando..."
                />
              </div>
            </form>
            { this.renderShippingOptions() }
            { this.renderCartWithShipping() }
          </Col>
        </Grid>

        <div>
          <h4>Métodos de Pagamento</h4>
        </div>
        { this.renderPaymentOptions() }
      </Fragment>
    );
  }
}
