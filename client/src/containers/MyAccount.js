import React, { Component, Fragment } from "react";
import { Button, Col, ControlLabel, Form, FormGroup, FormControl, Grid, Panel, Row } from "react-bootstrap";
import MaskedFormControl from 'react-bootstrap-maskedinput'
import LoaderButton from "../components/LoaderButton";
import { addLog } from '../logUtils';
import "./MyAccount.css";

export default class MyAccount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Loading states
      personalInformationLoading: false,
      addressesLoading : false,

      personalInformationChangeLoading: false,
      addressAdditionLoading: false,
      addressRemovalLoading: false,
      passwordChangeLoading: false,

      // User Data, will be retrieved from local storage
      cpf: "",
      email: "",
      sessionToken: "",

      // Account Data, will be loaded from external API
      name: "",
      changeName: false,
      dateOfBirth: "",
      changeDateOfBirth: false,
      phone: "",
      changePhone: false,

      // User Addresses, will be loaded from external API
      addresses: [],

      // New Address Data
      addressFetched: false,
      address: "",
      cep: "",
      number: "",
      complement: "",
      district: "",
      refference: "",
      city: "",
      state: "",

      // Password Data
      currentPassword: "",
      newPassword: "",
      confirmNewPassword: "",
    };
  }

  componentDidMount() {
    /* Load necessary information from local storage, which were added
       on Login */
    this.state.cpf = localStorage.getItem("userCpf");
    this.state.email = localStorage.getItem("loggedUser");
    this.state.sessionToken = localStorage.getItem("sessionToken");

    /* Load account information */
    this.loadPersonalInformation();
    this.loadAddresses();
  }

  /* Load personal information from external API (name, date of birth and
     phone) */
  loadPersonalInformation = async event => {
    this.setState({ personalInformationLoading: true });

    var loadedName, loadedDateOfBirth, loadedPhone;
    try {
      const request = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          tokenSessao: this.state.sessionToken
        })
      };

      await fetch("http://localhost:3002/users/" + this.state.cpf, request)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        loadedName = data.nome;
        loadedDateOfBirth = data.dataDeNascimento;
        loadedPhone = data.telefone;
      });
    } catch (e) {
      alert(e);
    }

    this.state.name = loadedName;
    this.state.dateOfBirth = loadedDateOfBirth;
    this.state.phone = loadedPhone;
    this.setState({ personalInformationLoading: false });
  }

  /* Load user addresses from external API */
  loadAddresses = async event => {
    this.setState({ addressesLoading: true });

    var loadedAddresses = {}
    try {
      await fetch("http://localhost:3002/users/" + this.state.cpf + "/addresses")
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        loadedAddresses = data;
      });
    } catch (e) {
      alert(e);
    }

    this.state.addresses = loadedAddresses;
    this.setState({ addressesLoading: false });
  }

  /* Auxiliar function */
  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  /********************************
   * Personal information methods *
   ********************************/
  renderPersonalInformationChangeForm() {
    return (
      <Form horizontal onSubmit={this.handlePersonalInformationChangeSubmit}>
        <h3>
          Editar dados
        </h3>
        <Panel>
          <Panel.Body>
            <FormGroup controlId="name">
              <Col componentClass={ControlLabel} sm={4}>
                Nome
              </Col>
              <Col sm={7}>
                <FormControl
                  type="text"
                  value={this.state.name}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="dateOfBirth">
              <Col componentClass={ControlLabel} sm={4}>
                Data de nascimento
              </Col>
              <Col sm={3}>
                <FormControl
                  type="text"
                  value={this.state.dateOfBirth}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="phone">
              <Col componentClass={ControlLabel} sm={4}>
                Telefone
              </Col>
              <Col sm={4}>
                <FormControl
                  type="text"
                  value={this.state.phone}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <Col sm={3} smOffset={9}>
              <LoaderButton
                block
                bsStyle="success"
                disabled={!this.validatePersonalInformationChange()}
                type="submit"
                isLoading={this.state.personalInformationChangeLoading}
                text="Salvar"
                loadingText="Salvando..."
              />
            </Col>
          </Panel.Body>
        </Panel>
      </Form>
    );
  }

  validatePersonalInformationChange() {
    return (
      this.state.name.length > 0
      && this.state.dateOfBirth.length > 0
      && this.state.phone.length > 0
    );
  }

  handlePersonalInformationChangeSubmit = async event => {
    this.setState({ personalInformationChangeLoading: true });

    var responseStatus;
    try {
      const request = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          tokenSessao: this.state.sessionToken,
          nome: this.state.name,
          email: this.state.email,
          dataDeNascimento: this.state.dateOfBirth,
          telefone: this.state.phone
        })
      };

      await fetch("http://localhost:3002/users/" + this.state.cpf + "/update", request)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
      .then(function (data) {
        if (responseStatus === 200) {
          addLog('Dados de ' + this.state.email + ' atualizados com sucesso',
                 'minhaconta');
          alert("Dados alterados com sucesso!");
        }
      });
    } catch (e) {
      addLog('Erro ao atualizar dados de ' + this.state.email, + ': ' +
             e.message + 'minhaconta');
      alert(e);
    }

    this.setState({ personalInformationChangeLoading: false });
  }

  /********************************
   * Add/remove addresses methods *
   ********************************/
  renderAddressesChangeForm() {
    const formattedAddresses = this.state.addresses.length > 0
    ? this.state.addresses.map((a) =>
        <form onSubmit={this.handleAddressRemovalSubmit} key={a.id}>
          <Grid fluid>
            <Panel>
              <Panel.Body>
                <Row className="show-grid" align="start">
                  <Col sm={8}>
                    <Panel>
                      <Panel.Heading>
                        Rua
                      </Panel.Heading>
                      <Panel.Body>
                        {a.rua}
                      </Panel.Body>
                    </Panel>
                  </Col>
                  <Col sm={4}>
                  <Panel>
                      <Panel.Heading>
                        CEP
                      </Panel.Heading>
                      <Panel.Body>
                        {a.cep}
                      </Panel.Body>
                    </Panel>
                  </Col>
                </Row>

                {a.complemento
                  ? <Row className="show-grid" align="start">
                      <Col sm={4}>
                        <Panel>
                          <Panel.Heading>
                            Número
                          </Panel.Heading>
                          <Panel.Body>
                            {a.numeroCasa}
                          </Panel.Body>
                        </Panel>
                      </Col>
                      <Col sm={4}>
                        <Panel>
                          <Panel.Heading>
                            Complemento
                          </Panel.Heading>
                          <Panel.Body>
                            {a.complemento}
                          </Panel.Body>
                        </Panel>
                      </Col>
                      <Col sm={4}>
                        <Panel>
                          <Panel.Heading>
                            Bairro
                          </Panel.Heading>
                          <Panel.Body>
                            {a.bairro}
                          </Panel.Body>
                        </Panel>
                      </Col>
                    </Row>
                  : <Row className="show-grid" align="start">
                      <Col sm={6}>
                        <Panel>
                          <Panel.Heading>
                            Número
                          </Panel.Heading>
                          <Panel.Body>
                            {a.numeroCasa}
                          </Panel.Body>
                        </Panel>
                      </Col>
                      <Col sm={6}>
                        <Panel>
                          <Panel.Heading>
                            Bairro
                          </Panel.Heading>
                          <Panel.Body>
                            {a.bairro}
                          </Panel.Body>
                        </Panel>
                      </Col>
                    </Row>
                }

                {a.referencia
                  ? <Row className="show-grid" align="start">
                      <Col sm={12}>
                      <Panel>
                          <Panel.Heading>
                            Referência
                          </Panel.Heading>
                          <Panel.Body>
                            {a.referencia}
                          </Panel.Body>
                        </Panel>
                      </Col>
                    </Row>
                  : null
                }

                <Row className="show-grid" align="start">
                  <Col sm={4}>
                    <Panel>
                      <Panel.Heading>
                        Cidade
                      </Panel.Heading>
                      <Panel.Body>
                        {a.cidade}
                      </Panel.Body>
                    </Panel>
                  </Col>
                  <Col sm={4}>
                    <Panel>
                      <Panel.Heading>
                        Estado
                      </Panel.Heading>
                      <Panel.Body>
                        {a.estado}
                      </Panel.Body>
                    </Panel>
                  </Col>
                  <Col sm={4}>
                    <Panel>
                      <Panel.Heading>
                        País
                      </Panel.Heading>
                      <Panel.Body>
                        Brasil
                      </Panel.Body>
                    </Panel>
                  </Col>
                </Row>

                <Row className="show-grid">
                  <Col sm={3} smOffset={9}>
                    <LoaderButton
                      id={a.id}
                      block
                      bsStyle="danger"
                      type="submit"
                      isLoading={this.state.addressRemovalLoading}
                      text="Remover"
                      loadingText="Removendo..."
                    />
                  </Col>
                </Row>
              </Panel.Body>
            </Panel>
          </Grid>
        </form>
      )
    : <div>
        Nenhum endereço adicionado!
      </div>;

    return (
      <Fragment>
        <form>
          <h3>
            Seus endereços
          </h3>
        </form>
        {formattedAddresses}

        {this.state.addressFetched === true
          ? <form onSubmit={this.handleAddressAdditionSubmit}>
              <h3>
                Novo endereço
              </h3>
              <Panel>
                <Panel.Body>
                  <Row className="show-grid" align="start">
                    <Col sm={8}>
                      <Panel>
                        <Panel.Heading>
                          Rua
                        </Panel.Heading>
                        <FormGroup controlId="address">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.address}
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>

                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          CEP
                        </Panel.Heading>
                        <FormGroup controlId="cep">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.cep}
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>
                  </Row>

                  <Row className="show-grid" align="start">
                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          Número
                        </Panel.Heading>
                        <FormGroup controlId="number">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.number}
                                onChange={this.handleChange}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>

                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          Complemento
                        </Panel.Heading>
                        <FormGroup controlId="complement">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.complement}
                                onChange={this.handleChange}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>

                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          Bairro
                        </Panel.Heading>
                        <FormGroup controlId="district">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.district}
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>
                  </Row>

                  <Row className="show-grid" align="start">
                    <Col sm={12}>
                      <Panel>
                        <Panel.Heading>
                          Referência
                        </Panel.Heading>
                        <FormGroup controlId="refference">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.refferece}
                                onChange={this.handleChange}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>
                  </Row>

                  <Row className="show-grid" align="start">
                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          Cidade
                        </Panel.Heading>
                        <FormGroup controlId="city">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.city}
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>

                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          Estado
                        </Panel.Heading>
                        <FormGroup controlId="state">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value={this.state.state}
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>

                    <Col sm={4} align="start">
                      <Panel>
                        <Panel.Heading>
                          País
                        </Panel.Heading>
                        <FormGroup controlId="country">
                          <Panel.Body>
                              <FormControl
                                type="text"
                                value="Brasil"
                                readOnly={true}
                              />
                          </Panel.Body>
                        </FormGroup>
                      </Panel>
                    </Col>
                  </Row>

                  <Row className="show-grid">
                    <Col sm={3} smOffset={6}>
                      <Button
                        block
                        bsStyle="warning"
                        onClick={() => this.cancelAddressAddition()}
                      >
                        Cancelar
                      </Button>
                    </Col>
                    <Col sm={3}>
                      <LoaderButton
                        block
                        bsStyle="success"
                        disabled={!this.validateAddressAddition()}
                        type="submit"
                        isLoading={this.state.addressAdditionLoading}
                        text="Adicionar"
                        loadingText="Adicionando…"
                      />
                    </Col>
                  </Row>
                </Panel.Body>
              </Panel>
            </form>
          : <form onSubmit={this.handleAddressCepSearchSubmit}>
              <h3>
                Novo endereço
              </h3>
                <Row className="show-grid" align="start">
                  <Col sm={4} align="start">
                    <Panel>
                      <Panel.Heading>
                        CEP
                      </Panel.Heading>
                      <FormGroup controlId="cep">
                        <Panel.Body>
                          <MaskedFormControl
                            type='text'
                            value={this.state.cep}
                            onChange={this.handleChange}
                            mask='11111-111'
                          />
                          <br />
                          <LoaderButton
                            block
                            bsStyle="success"
                            disabled={!this.validateAddressCepSearch()}
                            type="submit"
                            isLoading={this.state.addressAdditionLoading}
                            text="Procurar"
                            loadingText="Procurando..."
                          />
                        </Panel.Body>
                      </FormGroup>
                    </Panel>
                  </Col>
                </Row>
            </form>}
      </Fragment>
    );
  }

  handleAddressRemovalSubmit = async event => {
    this.setState({ addressRemovalLoading: true });

    var responseStatus;
    try {
      const request = {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          tokenSessao: this.state.sessionToken,
          id: event.target.elements[0].id
        })
      };

      await fetch("http://localhost:3002/addresses/" + this.state.cpf + "/remove", request)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
      .then(function (data) {
        if (responseStatus === 200) {
           addLog('Endereço de ' + this.state.email + ' removido com sucesso',
                  'minhaconta');
          alert("Endereço removido com sucesso!");
        }
      });
    } catch (e) {
      addLog('Erro ao tentar remover endereço de ' + this.state.email,
             'minhaconta');
      alert(e);
    }

    this.setState({ addressRemovalLoading: false });
  }

  validateAddressCepSearch() {
    return (
      this.state.cep.length === 9
    );
  }

  handleAddressCepSearchSubmit = async event => {
    event.preventDefault();
    this.setState({ addressAdditionLoading: true });

    var responseStatus;
    var endereco = false;
    var encontrou;
    var cep = this.state.cep.split("-").join("");
    try {
      await fetch("http://wsendereco.tk/api/enderecos/cep/" + cep)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
      .then(function (data) {
        if (data.Endereco && data.Endereco.length > 0) {
          endereco = data.Endereco[0];
          encontrou = true;
        }
      })
    } catch(e) {
      alert(e);
      return;
    }

    if (encontrou === true && responseStatus === 200) {
      this.state.addressFetched = true;
      this.state.address = endereco.logradouro;
      this.state.district = endereco.bairro;
      this.state.city = endereco.cidade;
      this.state.state = endereco.estado;
      addLog('Dados de CEP ' + cep + ' obtidos com sucesso', 'minhaconta');
    } else {
      addLog('Erro: CEP ' + cep + ' não encontrado', 'minhaconta');
      alert("CEP não encontrado! Por favor, entre com um CEP válido.");
    }

    this.setState({ addressAdditionLoading: false });
  }

  validateAddressAddition() {
    return (
      this.state.number.length > 0
    );
  }

  cancelAddressAddition() {
    this.state.address = "";
    this.state.district = "";
    this.state.city = "";
    this.state.state = "";
    this.setState({ addressFetched: false });
  }

  handleAddressAdditionSubmit = async event => {
    this.setState({ addressAdditionLoading: true });

    var responseStatus;
    try {
      const request = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          tokenSessao: this.state.sessionToken,
          cep: this.state.cep.split("-").join(""),
          rua: this.state.address,
          numeroCasa: this.state.number,
          bairro: this.state.district,
          complemento: this.state.complement,
          referencia: this.state.refference,
          cidade: this.state.city,
          estado: this.state.state
        })
      };

      await fetch("http://localhost:3002/addresses/" + this.state.cpf + "/add", request)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
      .then(function (data) {
        if (responseStatus === 200) {
          addLog('Endereço adicionado com sucesso para usuário ' +
                 this.state.email, 'minhaconta');
          alert("Endereço adicionado com sucesso!");
        } else {
          addLog('Erro ao adicionar endereço: ' + data.message, 'minhaconta');
          alert("Erro ao adicionar o endereço: " + data.message);
        }
      });
    } catch (e) {
      alert(e);
    }

    this.state.addressFetched = false;
    this.setState({ addressAdditionLoading: false });
  }

  /***************************
   * Password change methods *
   ***************************/
  renderPasswordChangeForm() {
    return (
      <Form horizontal onSubmit={this.handlePasswordChangeSubmit}>
        <h3>
          Alterar senha
        </h3>
        <Panel>
          <Panel.Body>
            <FormGroup controlId="currentPassword">
              <Col componentClass={ControlLabel} sm={3}>
                Senha atual
              </Col>
              <Col sm={8}>
                <FormControl
                  type="password"
                  value={this.state.currentPassword}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="newPassword">
              <Col componentClass={ControlLabel} sm={3}>
                Nova senha
              </Col>
              <Col sm={8}>
                <FormControl
                  type="password"
                  value={this.state.newPassword}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="confirmNewPassword">
              <Col componentClass={ControlLabel} sm={3}>
                Confirmar senha
              </Col>
              <Col sm={8}>
                <FormControl
                  type="password"
                  value={this.state.confirmNewPassword}
                  onChange={this.handleChange}
                />
              </Col>
            </FormGroup>

            <FormGroup controlId="submitPasswordChanges">
              <Col sm={3} smOffset={9}>
                <LoaderButton
                  block
                  bsStyle="success"
                  disabled={!this.validatePasswordChange()}
                  type="submit"
                  isLoading={this.state.passwordChangeLoading}
                  text="Salvar"
                  loadingText="Salvando…"
                />
              </Col>
            </FormGroup>
          </Panel.Body>
        </Panel>
      </Form>
    );
  }

  validatePasswordChange() {
    return (
      this.state.currentPassword.length > 0
      && this.state.newPassword.length > 0
      && this.state.confirmNewPassword > 0
      && (this.state.newPassword === this.state.confirmNewPassword)
    );
  }

  handlePasswordChangeSubmit = async event => {
    alert("Funcionalidade ainda não implementada!");
  }

  /* Full component render */
  render() {
    return (
      <div className="MyAccount">
        {this.state.cpf
          ? <Fragment>
              { this.renderPersonalInformationChangeForm() }
              <hr />
              { this.renderAddressesChangeForm() }
              <hr />
              { this.renderPasswordChangeForm() }
            </Fragment>
          : <div>
              Você não está logado! Por favor, faça login para ver as informações.
            </div>
        }
      </div>
    );
  }
}
