import React, { Component } from "react";
import { Col, ControlLabel, Form, FormGroup, FormControl, Panel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";

import "./Contact.css";

export default class Contact extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  handleSubmit = async event => {
    alert("Mensagem enviada com sucesso!");
    this.setState({ isLoading: false });
  }

  render() {
    return (
      <div className="Contact">
        <Form horizontal onSubmit={this.handleSubmit} >
          <h3>
            Contato
          </h3>
          <Panel>
            <Panel.Body>
              <FormGroup controlId="name">
                <Col componentClass={ControlLabel} sm={3}>
                  Nome
                </Col>
                <Col sm={7}>
                  <FormControl
                    type="text"
                  />
                </Col>
              </FormGroup>

              <FormGroup controlId="email">
                <Col componentClass={ControlLabel} sm={3}>
                  E-mail
                </Col>
                <Col sm={7}>
                  <FormControl
                    type="text"
                  />
                </Col>
              </FormGroup>

              <FormGroup controlId="phone">
                <Col componentClass={ControlLabel} sm={3}>
                  Telefone
                </Col>
                <Col sm={4}>
                  <FormControl
                    type="text"
                  />
                </Col>
              </FormGroup>

              <FormGroup controlId="message">
                <Col componentClass={ControlLabel} sm={3}>
                  Mensagem
                </Col>
                <Col sm={7}>
                  <FormControl
                    componentClass="textarea"
                    type="textarea"
                  />
                </Col>
              </FormGroup>

              <Col sm={3} smOffset={8}>
                <LoaderButton
                  block
                  bsStyle="success"
                  isLoading={this.state.isLoading}
                  type="submit"
                  text="Enviar"
                  loadingText="Enviando..."
                />
              </Col>
            </Panel.Body>
          </Panel>
          </Form>
        </div>

    );
  }
}
