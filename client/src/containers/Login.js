import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { addLog } from '../logUtils';
import "./Login.css";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  /* On login button click, call an API to authenticate the user (which returns
     the generated session token, to be stored on browser) */
  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ isLoading: true });

    var responseStatus;
    try {
      const loginRequest = {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: this.state.email,
          senha: this.state.password
        })
      };

      var sessionToken;
      await fetch("http://localhost:3002/login", loginRequest)
      .then(function (response) {
        responseStatus = response.status;
        return response.json();
      })
      .then(function (data) {
        if (responseStatus !== 200) {
          addLog('Erro ao fazer login: ' + data.message, 'login');
          alert("Erro ao fazer login: " + data.message);
        } else {
          sessionToken = data.sessionToken;
        }
      });

      var loggedUser = this.state.email;
      var userCpf, isAdmin, activeUser;
      if (responseStatus === 200) {
        addLog('Usuário ' + this.state.email + ' fez login com sucesso', 'login');
        localStorage.setItem("loggedUser", this.state.email);
        localStorage.setItem("sessionToken", sessionToken);

        await fetch("http://localhost:3001/users/" + this.state.email)
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          userCpf = data.cpf;
          isAdmin = data.isAdmin;
        })

        const informationRequest = {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            tokenSessao: sessionToken
          })
        };

        await fetch("http://localhost:3002/users/" + userCpf, informationRequest)
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          activeUser = data.ativo;
          localStorage.setItem("userCpf", data.cpf);
          localStorage.setItem("isAdmin", isAdmin);
        })
      }
    } catch (e) {
      addLog('Erro ao realizar login: ' + e.message, 'login');
      alert(e);
    }

    this.setState({ isLoading: false });

    if (responseStatus === 200) {
      if (activeUser === true) {
        this.props.userHasAuthenticated(true);
        localStorage.setItem("loggedUser", loggedUser);
        localStorage.setItem("sessionToken", sessionToken);
        localStorage.setItem("userCpf", userCpf);
        this.props.userIsAdmin(isAdmin);
        localStorage.setItem("isAdmin", isAdmin);
        this.props.history.push("/");
      } else {
        localStorage.setItem("confirmedUser", false);
        this.props.history.push("/cadastrar");
      }
    }
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
              <FormControl
                value={this.state.password}
                onChange={this.handleChange}
                type="password"
              />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            loadingText="Fazendo login..."
          />
        </form>
      </div>
    );
  }
}
