import React from "react";
import { Route, Switch } from "react-router-dom";
import AppliedRoute from "./components/AppliedRoute";
import Home from "./containers/Home";
import Login from "./containers/Login";
import NotFound from "./containers/NotFound";
import Signup from "./containers/Signup";
import ProductRegister from "./containers/ProductRegister";
import SearchProduct from "./containers/SearchProduct";
import AboutUs from "./containers/AboutUs";
import Acessibility from "./containers/Acessibility";
import Policy from "./containers/Policy";
import Stores from "./containers/Stores";
import Contact from "./containers/Contact";
import MyAccount from "./containers/MyAccount";
import ShoppingCart from "./containers/ShoppingCart";
import Offers from "./containers/Offers";
import CellPhones from "./containers/CellPhones";
import HomeAppliances from "./containers/HomeAppliances";
import Furniture from "./containers/Furniture";
import Computing from "./containers/Computing";
import Payment from "./containers/Payment";
import Logs from "./containers/Logs";

export default ({ childProps }) =>
  <Switch>
    <AppliedRoute path="/" exact component={Home} props={childProps} />
    <AppliedRoute path="/login" exact component={Login} props={childProps} />
    <AppliedRoute path="/cadastrar" exact component={Signup}
      props={childProps} />
    <AppliedRoute path="/cadastrarProduto" exact component={ProductRegister}
      props={childProps} />
    <AppliedRoute path="/buscarProduto/:nome" exact component={SearchProduct}
      props={childProps} />
    <AppliedRoute path="/quemSomos" exact component={AboutUs}
      props={childProps} />
    <AppliedRoute path="/acessibilidade" exact component={Acessibility}
      props={childProps}/>
    <AppliedRoute path="/politicas" exact component={Policy}
      props={childProps} />
    <AppliedRoute path="/lojas" exact component={Stores}
      props={childProps} />
    <AppliedRoute path="/contato" exact component={Contact}
     props={childProps} />
     <AppliedRoute path="/carrinho" exact component={ShoppingCart}
      props={childProps} />
    <AppliedRoute path="/pagamento" exact component={Payment}
      props={childProps} />
    <AppliedRoute path="/minhaConta" exact component={MyAccount}
      props={childProps} />
    <AppliedRoute path="/logs" exact component={Logs} />
    <AppliedRoute path="/filtro-ofertas" exact component={Offers}
      props={childProps} />
    <AppliedRoute path="/filtro-celular" exact component={CellPhones}
      props={childProps} />
    <AppliedRoute path="/filtro-eletrodomesticos" exact component={HomeAppliances}
      props={childProps} />
    <AppliedRoute path="/filtro-informatica" exact component={Computing}
      props={childProps} />
    <AppliedRoute path="/filtro-moveis" exact component={Furniture}
      props={childProps} />
    <Route component={NotFound} />
  </Switch>;
