'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var LogSchema = new Schema({
    text: {type: String, required: true},
    tag: {type: String, required: true},
    timestamp: {type: Number, required: true}
});

var LogModel = mongoose.model('logs', LogSchema);
module.exports = LogModel;
