'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var UserSchema = new Schema({
    cpf: {type: String, required: true, index: true},
    email: {type: String, required: true},
    senha: {type: String, required: true},
    tokenSessao: String,

    // To activate admin status, run (on mongo console):
    // db.users.findOneAndUpdate({"email": "[ADMIN EMAIL]"}, {$set: {"isAdmin": true}})
    isAdmin: {type: Boolean, required: true},
});

var UserModel = mongoose.model('users', UserSchema);
module.exports = UserModel;
