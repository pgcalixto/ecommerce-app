'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var OrderSchema = new Schema({
  cpf: {type: String, required: true, index: true},
  productList: {type: [{
    productId: {type: String, required: true},
    quantity: {type: Number, required: true}
  }], required: true}
});

var OrderModel = mongoose.model('orders', OrderSchema);
module.exports = OrderModel;
